<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/persons', function (Request $request, Response $response, $args) {
    $q = $request->getQueryParam('q');

    try{
        if (empty($q)) {
            $stmt = $this->db->prepare('SELECT * 
                                FROM person
                                ORDER BY last_name');
        } else{
            $stmt = $this->db->prepare('SELECT * 
                                FROM person
                                WHERE last_name ILIKE :q                            
                                ORDER BY last_name');
            $stmt->bindValue(':q', $q . '%');
        }
        $stmt->execute();
        $tplVars['people'] = $stmt->fetchAll();
        return $this->view->render($response, 'persons.latte',$tplVars);
    } catch (Exception $e){
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('persons');

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/new-person',function (Request $request, Response $response, $args){
    return $this->view->render($response,'new-person.latte');
})->setName('newPerson');

$app->post('/new-person',function (Request $request, Response $response, $args){
    $data = $request->getParsedBody(); //ziskanie dat
    if(!empty($data['fn']&& !empty($data['ln']) && !empty($data['nn']))) {
        try {
            $stmt = $this->db->prepare('INSERT INTO person(first_name, last_name, nickname)
                                    VALUES 
                                    (:fn, :ln, :nn)');
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $v = empty($data['h']) ? null : $data['h'];
            $stmt->bindValue(':h', $h);
            $stmt->execute();


        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            die($e->getMessage());
        }

        return $response->withHeader('Location', $this->router->pathFor('persons'));
    } else{
        return $this->view->render($response, 'new-person.latte');
    }


});
